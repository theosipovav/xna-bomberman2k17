﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BomberMan2k17
{
    class Buttons
    {
        private Texture2D _Texture;
        private Vector2 _Position, _OriginalPosition;
        private Rectangle _Rectangle, _RectangleInteraction, _RectangleMouse;
        private Color _Colour = new Color(255, 255, 255, 255);
        private int _FrameHeight, _FrameWidth, _FrameCurrent;
        public Buttons(Texture2D newTexture, Vector2 newPosition)
        {
            _Texture = newTexture;
            _Position = newPosition;
            _FrameHeight = _Texture.Height;
            _FrameWidth = _Texture.Width / 2;
        }
        public bool isClicked;
        public void Update(MouseState _InMouseState)
        {
            _RectangleMouse = new Rectangle(_InMouseState.X, _InMouseState.Y, 1, 1);
            _Rectangle = new Rectangle(_FrameCurrent * _FrameWidth, 0, _FrameWidth, _FrameHeight);
            _RectangleInteraction = new Rectangle((int)_Position.X - _FrameWidth / 2, (int)_Position.Y - _FrameHeight / 2, _FrameWidth, _FrameHeight);
            _OriginalPosition = new Vector2(_Rectangle.Width / 2, _Rectangle.Height / 2);
            if (_RectangleMouse.Intersects(_RectangleInteraction))
            {
                _FrameCurrent = 1;
                if (_InMouseState.LeftButton == ButtonState.Pressed) isClicked = true;
            }
            else
            {
                _FrameCurrent = 0;
                isClicked = false;
            }
        }
        public void Draw(SpriteBatch _SpriteBatch)
        {
            _SpriteBatch.Draw(_Texture, _Position, _Rectangle, _Colour, 0f, _OriginalPosition, 1.0f, SpriteEffects.None, 0.1f);
        }

        public void toMod_Position(Vector2 newMod)
        {
            _Position = newMod;
        }
    }

}
