﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace BomberMan2k17
{
    class Player
    {
        private Texture2D _Texture;
        private Rectangle _Rectangle, _RectangleInteraction;
        private Vector2 _PositionOriginal, _Position, _PositionEnd1, _PositionEnd2, _Velosity;
        private KeyboardState _KeyboardState;
        private Color _Colour;
        private int _FrameHeight, _FrameWidth, _FrameCurrent;
        private float Timer, TimerImmunity, Interval = 100;
        private bool down, FlagImmunity;

        private string _Name;
        private int _Live;

        private bool FlagPressW, FlagPressA, FlagPressS, FlagPressD;
        private bool FlagPrePressW, FlagPrePressA, FlagPrePressS = true, FlagPrePressD;


        private struct Attribution
        {
            public int _HP;
            public int _Speed;
            public string _Status;
        } Attribution _Attribution;

        public Player(Texture2D newTexture, Vector2 newPosition)
        {
            _Texture = newTexture;
            _Position = newPosition;
            _Colour = new Color(255, 255, 255, 255);
            _FrameHeight = 30;
            _FrameWidth = 30;
            _Attribution._HP = 3;
            _Attribution._Speed = 3;
            _Attribution._Status = "Live";
            FlagImmunity = false;
            _Name = "Hero";
            _Live = 5;
            TimerImmunity = 0;
        }

        public void Update(GameTime _GameTime)
        {
            _Rectangle = new Rectangle(_FrameCurrent * _FrameWidth, 0, _FrameWidth, _FrameHeight);
            _PositionOriginal = new Vector2(_Rectangle.Width / 2, _Rectangle.Height / 2);
            _RectangleInteraction = new Rectangle((int)_Position.X - _FrameWidth, (int)_Position.Y, _FrameWidth, _FrameHeight);
            _KeyboardState = Keyboard.GetState();
            _PositionEnd1 = _Position;
            if (_PositionEnd1 == _PositionEnd2)
            {
                FlagPressW = false;
                FlagPressA = false;
                FlagPressS = false;
                FlagPressD = false;
            }
            if ((_KeyboardState.IsKeyDown(Keys.Up)) && ((!FlagPressA) && (!FlagPressS) && (!FlagPressD)))
            {
                FlagPressW = true;
                Top(_GameTime);
                _Velosity.Y = -_Attribution._Speed;
                _Position.Y = _Position.Y + _Velosity.Y;
            }
            if ((_KeyboardState.IsKeyDown(Keys.Left)) && ((!FlagPressW) && (!FlagPressS) && (!FlagPressD)))
            {
                FlagPressA = true;
                Left(_GameTime);
                _Velosity.X = -_Attribution._Speed;
                _Position.X = _Position.X + _Velosity.X;
            }
            if ((_KeyboardState.IsKeyDown(Keys.Down)) && ((!FlagPressW) && (!FlagPressA) && (!FlagPressD)))
            {
                FlagPressS = true;
                Botton(_GameTime);
                _Velosity.Y = _Attribution._Speed;
                _Position.Y = _Position.Y + _Velosity.Y;
            }
            if ((_KeyboardState.IsKeyDown(Keys.Right)) && ((!FlagPressW) && (!FlagPressA) && (!FlagPressS)))
            {
                FlagPressD = true;
                Right(_GameTime);
                _Velosity.X = _Attribution._Speed;
                _Position.X = _Position.X + _Velosity.X;
            }

            if ((!(_KeyboardState.IsKeyDown(Keys.Up))) && (!(_KeyboardState.IsKeyDown(Keys.Left))) && (!(_KeyboardState.IsKeyDown(Keys.Down))) && (!(_KeyboardState.IsKeyDown(Keys.Right))) && (!(_KeyboardState.IsKeyDown(Keys.E))))
            {
                _FrameCurrent = 0;
                _Velosity = Vector2.Zero;
            }
            _PositionEnd2 = _Position;
            if (_Attribution._HP <= 0) _Attribution._Status = "Dead";

            if (FlagImmunity)
            {
                TimerImmunity += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
                if (_Colour.A == 255) down = false;
                if (_Colour.A == 0) down = true;
                if (down)
                {
                    _Colour.R += 15;
                    _Colour.G += 15;
                    _Colour.B += 15;
                    _Colour.A += 15;
                }
                else
                {
                    _Colour.R -= 15;
                    _Colour.G -= 15;
                    _Colour.B -= 15;
                    _Colour.A -= 15;
                }
                if (TimerImmunity > 2000)
                    FlagImmunity = false;
            }
            else if (_Colour.A < 255) _Colour = new Color(255, 255, 255, 255);
        }

        public void Draw(SpriteBatch _SpriteBatch)
        {
            _SpriteBatch.Draw(_Texture, _Position, _Rectangle, _Colour, 0f, _PositionOriginal, 1.0f, SpriteEffects.None, 0.5f);
        }
        public void Top(GameTime Gametime)
        {
            FlagPrePressA = FlagPrePressS = FlagPrePressD = false;
            FlagPrePressW = true;
            Timer += (float)Gametime.ElapsedGameTime.TotalMilliseconds;
            if (Timer > Interval)
            {
                _FrameCurrent++;
                Timer = 0;
                if ((_FrameCurrent < 9) || (_FrameCurrent > 11)) _FrameCurrent = 9;
            }
        }
        public void Right(GameTime Gametime)
        {
            FlagPrePressW = FlagPrePressA = FlagPrePressS = false;
            FlagPrePressD = true;
            Timer += (float)Gametime.ElapsedGameTime.TotalMilliseconds;
            if (Timer > Interval)
            {
                _FrameCurrent++;
                Timer = 0;
                if ((_FrameCurrent < 6) || (_FrameCurrent > 8)) _FrameCurrent = 6;
            }
        }
        public void Left(GameTime Gametime)
        {
            FlagPrePressW = FlagPrePressS = FlagPrePressD = false;
            FlagPrePressA = true;
            Timer += (float)Gametime.ElapsedGameTime.TotalMilliseconds;
            if (Timer > Interval)
            {
                _FrameCurrent++;
                Timer = 0;
                if ((_FrameCurrent < 3) || (_FrameCurrent > 5)) _FrameCurrent = 3;
            }
        }
        public void Botton(GameTime Gametime)
        {
            FlagPrePressW = FlagPrePressA = FlagPrePressD = false;
            FlagPrePressS = true;
            Timer += (float)Gametime.ElapsedGameTime.TotalMilliseconds;
            if (Timer > Interval)
            {
                _FrameCurrent++;
                Timer = 0;
                if (_FrameCurrent > 2) _FrameCurrent = 0;
            }
        }

        public bool Interaction(Rectangle newRectangle_Interaction)
        {
            if (newRectangle_Interaction.Contains(Convert.ToInt32(_Position.X - 10), Convert.ToInt32(_Position.Y - 14))) return true;
            if (newRectangle_Interaction.Contains(Convert.ToInt32(_Position.X - 10), Convert.ToInt32(_Position.Y + 14))) return true;
            if (newRectangle_Interaction.Contains(Convert.ToInt32(_Position.X + 10), Convert.ToInt32(_Position.Y - 14))) return true;
            if (newRectangle_Interaction.Contains(Convert.ToInt32(_Position.X + 10), Convert.ToInt32(_Position.Y + 14))) return true;
            return false;
        }
        public bool InteractionIn(Vector2 _newObjPosition)
        {
            if (_RectangleInteraction.Contains((int)_newObjPosition.X, (int)_newObjPosition.Y)) return true;
            return false;
        }



        public void StopVelosity()
        {
            if (FlagPrePressW == true) _Position.Y += _Attribution._Speed;
            if (FlagPrePressA == true) _Position.X += _Attribution._Speed;
            if (FlagPrePressS == true) _Position.Y -= _Attribution._Speed;
            if (FlagPrePressD == true) _Position.X -= _Attribution._Speed;
        }
        public void DMG(int _newDMG)
        {
            if (!FlagImmunity)
            {
                _Live -= _newDMG;
                FlagImmunity = true;
                TimerImmunity = 0;
            }
        }

        public void AddLive(int _newLive)
        {
            _Live += _newLive;
        }


        /* Доступ к private переменным */
        public Rectangle ToGet_RectangleInteraction() { return _RectangleInteraction; }
        public Vector2 ToGet_Position() { return _Position; }
        public string ToGet_Name() { return _Name; }
        public int ToGet_Live() { return _Live; }


        public void ToSet_Name(string _newName)
        {
            _Name = _newName;
        }

    }
}
