﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BomberMan2k17
{
    class Objects
    {
        protected Texture2D _Texture;
        protected Vector2 _Position, _OriginalPosition;
        protected Rectangle _Rectangle, _RectangleInteraction;
        protected Color _Colour;


        public Objects(Texture2D _newTexture, Vector2 _newPosition)
        {
            _Texture = _newTexture;
            _Position = _newPosition;
            _Colour = new Color(255, 255, 255, 255);
        }

        public Rectangle ToGet_RectangleInteraction() { return _RectangleInteraction; }

    }
    class Block : Objects
    {
        public Block(Texture2D _newTexture, Vector2 _newPosition) : base(_newTexture, _newPosition)
        { }
        public void Update()
        {
            _Rectangle = new Rectangle((int)_Position.X + _Texture.Width, (int)_Position.Y + _Texture.Height, _Texture.Width, _Texture.Height);
            _OriginalPosition = new Vector2(_Rectangle.Width, _Rectangle.Height);
            _RectangleInteraction = new Rectangle((int)_Position.X, (int)_Position.Y, _Texture.Width, _Texture.Height);
        }
        public void Draw(SpriteBatch _SpriteBatch)
        {
            _SpriteBatch.Draw(_Texture, _Rectangle, null, _Colour, 0f, _OriginalPosition, SpriteEffects.None, 0.4f);
        }
    }

    class BlockDestr : Objects
    {
        private bool _fExist;

        public BlockDestr(Texture2D _newTexture, Vector2 _newPosition) : base(_newTexture, _newPosition)
        {
            _fExist = true;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Update()
        {
            if (_fExist)
            {
                _Rectangle = new Rectangle((int)_Position.X + _Texture.Width, (int)_Position.Y + _Texture.Height, _Texture.Width, _Texture.Height);
                _OriginalPosition = new Vector2(_Rectangle.Width, _Rectangle.Height);
                _RectangleInteraction = new Rectangle((int)_Position.X, (int)_Position.Y, _Texture.Width, _Texture.Height);
            }
        }
        public void Draw(SpriteBatch _SpriteBatch)
        {
            if (_fExist)
            {
                _SpriteBatch.Draw(_Texture, _Rectangle, null, _Colour, 0f, _OriginalPosition, SpriteEffects.None, 0.4f);
            }
        }
        public bool Interaction(Vector2 _newObjPosition)
        {
            if (_RectangleInteraction.Contains((int)_newObjPosition.X, (int)_newObjPosition.Y)) return true;
            return false;
        }
        public void Dest()
        {
            _fExist = false;
        }
        /* Работа с private переменными */
        public void ToSet_fExist(bool _newflag) { _fExist = _newflag; }
        public bool ToGet_fExist() { return _fExist; }
    }

    class Bomb : Objects
    {
        /******************************************************************/
        /* Переменные взрыва */
        private Texture2D _TextureBoom;
        private Vector2 _PositionBoom, _OriginalPositionBoom;
        private Rectangle _RectangleBoom, _RectangleInteractionBoom;
        private int _FrameHeightBoom, _FrameWidthBoom, _FrameCurrentBoom;
        private Color _ColourBoom;
        /******************************************************************/

        private SpriteFont _SpriteFont;
        private int _FrameHeight, _FrameWidth, _FrameCurrent;
        private Vector2[] _TrackBoom;


        private bool _fActivate, _fBoom;
        private float  _TimerActivate, _TimerFrame;

        public Bomb(Texture2D _newTexture, Vector2 _newPosition, SpriteFont _newSpriteFont, Texture2D _newTextureBoom) : base(_newTexture, _newPosition)
        {
            _SpriteFont = _newSpriteFont;
            _FrameHeight = _FrameWidth = 32;
            _fActivate = false;
            _fBoom = false;
            _TimerFrame = 0;
            _TimerActivate = 4;
            _TrackBoom = new Vector2[5];



            _TextureBoom = _newTextureBoom;
            _PositionBoom = new Vector2(0,0);
            _ColourBoom = new Color(255, 255, 255, 255);

            _FrameHeightBoom = 70;
            _FrameWidthBoom = 70;

        }
        public void Update(GameTime _GameTime)
        {
            if (_fActivate)
            {
                /* Бомба */
                _Rectangle = new Rectangle(_FrameCurrent * _FrameWidth, 0, _FrameWidth, _FrameHeight);
                _OriginalPosition = new Vector2(_Rectangle.Width / 2, _Rectangle.Height / 2);
                _RectangleInteraction = new Rectangle((int)_Position.X - _FrameWidth / 2, (int)_Position.Y - _FrameHeight / 2, _FrameWidth, _FrameHeight);
                _TimerFrame += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
                if (_TimerFrame >= 500)
                {
                    if (_FrameCurrent == 0) _FrameCurrent = 1;
                    else _FrameCurrent = 0;
                    _TimerFrame = 0;
                }

                _TimerActivate -= (float)_GameTime.ElapsedGameTime.TotalSeconds;
                /* Логика взрыва */
                if (_TimerActivate < 1)
                {
                    _fBoom = true;
                }
                if (_fBoom)
                {
                    _TrackBoom[0] = _Position;
                    _TrackBoom[1] = new Vector2(_Position.X, _Position.Y - 32);
                    _TrackBoom[2] = new Vector2(_Position.X + 32, _Position.Y);
                    _TrackBoom[3] = new Vector2(_Position.X, _Position.Y + 32);
                    _TrackBoom[4] = new Vector2(_Position.X - 32, _Position.Y);
                    _PositionBoom = _Position;
                    _RectangleBoom = new Rectangle(_FrameCurrentBoom * _FrameWidthBoom, 0, _FrameWidthBoom, _FrameHeightBoom);
                    _OriginalPositionBoom = new Vector2(_RectangleBoom.Width / 2, _RectangleBoom.Height / 2);
                    _RectangleInteractionBoom = new Rectangle((int)_PositionBoom.X - _FrameWidthBoom / 2, (int)_PositionBoom.Y - _FrameHeightBoom / 2, _FrameWidthBoom, _FrameHeightBoom);
                    if ((_TimerActivate > 0.75f) && (_TimerActivate <= 1)) _FrameCurrentBoom = 0;
                    if ((_TimerActivate > 0.5) && (_TimerActivate <= 0.75f)) _FrameCurrentBoom = 1;
                    if ((_TimerActivate > 0.25f) && (_TimerActivate <= 0.5f)) _FrameCurrentBoom = 2;
                    if ((_TimerActivate > 0.25) && (_TimerActivate <= 0.5f)) _FrameCurrentBoom = 3;
                    if ((_TimerActivate > 0) && (_TimerActivate <= 0.25f)) _FrameCurrentBoom = 4;
                    if ((_TimerActivate > -0.75f) && (_TimerActivate <= 0)) _FrameCurrentBoom = 5;
                    if ((_TimerActivate > -0.5f) && (_TimerActivate <= -0.75f)) _FrameCurrentBoom = 6;
                    if ((_TimerActivate > -0.25f) && (_TimerActivate <= -0.5f)) _FrameCurrentBoom = 7;
                }
                /* Логика существование бомбы */
                if (_TimerActivate < -0.5f)
                {
                    _TimerActivate = 3;
                    _fActivate = false;
                    _fBoom = false;
                    _TrackBoom = new Vector2[5];
                    _FrameCurrentBoom = 0;
                }
            }
            
        }
        public void Draw(SpriteBatch _SpriteBatch)
        {
            if (_fActivate)
            {
                if (_TimerActivate > 1)
                {
                    _SpriteBatch.Draw(_Texture, _Position, _Rectangle, _Colour, 0f, _OriginalPosition, 1.0f, SpriteEffects.None, 0.39f);
                    _SpriteBatch.DrawString(_SpriteFont, ((int)_TimerActivate).ToString(), new Vector2(_Position.X - 8, _Position.Y - 12), Color.White, 0f, new Vector2(0, 0), 0.15f, SpriteEffects.None, 0.1f);
                }
                if (_TimerActivate < 1)
                {
                    _SpriteBatch.Draw(_TextureBoom, _PositionBoom, _RectangleBoom, _ColourBoom, 0f, _OriginalPositionBoom, 1.0f, SpriteEffects.None, 0.47f);
                }

            }
               
        }


        /* Работа с private переменными */
        public void ToSet_Position(Vector2 _newPosition) { _Position = _newPosition; }
        public void ToSet_fActivate(bool _newfActivate) { _fActivate = _newfActivate; }

        public bool ToGet_fBoom() { return _fBoom; }
        public bool ToGet_fActivate() { return _fActivate; }
        public Vector2[] ToGet_TrackBomb() { return _TrackBoom; }

    }

    class Portal : Objects
    {
        public Portal(Texture2D _newTexture, Vector2 _newPosition) : base(_newTexture, _newPosition)
        { }
        public void Update()
        {
            _Rectangle = new Rectangle((int)_Position.X + _Texture.Width, (int)_Position.Y + _Texture.Height, _Texture.Width, _Texture.Height);
            _OriginalPosition = new Vector2(_Rectangle.Width, _Rectangle.Height);
            _RectangleInteraction = new Rectangle((int)_Position.X, (int)_Position.Y, _Texture.Width, _Texture.Height);
        }
        public void Draw(SpriteBatch _SpriteBatch)
        {
            _SpriteBatch.Draw(_Texture, _Rectangle, null, _Colour, 0f, _OriginalPosition, SpriteEffects.None, 0.4f);
        }
    }

    class Enemy : Objects
    {
        private int _FrameHeight, _FrameWidth, _FrameCurrent;
        private Vector2[] _TrackBoom;
        private string _Direction;
        private float  _TimerFrame, _TimerVelosity;

        public Enemy(Texture2D _newTexture, Vector2 _newPosition) : base(_newTexture, _newPosition)
        {
            _FrameHeight = _FrameWidth = 32;
            _TimerFrame = 0;
            _TimerVelosity = 0;
            _Direction = "R";
        }
        public void Update(GameTime _GameTime)
        {
            /* Бомба */
            _Rectangle = new Rectangle(_FrameCurrent * _FrameWidth, 0, _FrameWidth, _FrameHeight);
            _OriginalPosition = new Vector2(_Rectangle.Width / 2, _Rectangle.Height / 2);
            _RectangleInteraction = new Rectangle((int)_Position.X - _FrameWidth / 2, (int)_Position.Y - _FrameHeight / 2, _FrameWidth, _FrameHeight);
            _TimerFrame += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
            if (_TimerFrame >= 500)
            {
                if (_FrameCurrent == 0) _FrameCurrent = 1;
                else _FrameCurrent = 0;
                _TimerFrame = 0;
            }
            

        }
        public void Draw(SpriteBatch _SpriteBatch)
        {
            _SpriteBatch.Draw(_Texture, _Position, _Rectangle, _Colour, 0f, _OriginalPosition, 1.0f, SpriteEffects.None, 0.39f);
        }
        public void Movement(GameTime _GameTime)
        {
            _TimerFrame += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
            if (_TimerFrame >= 30)
            {
                if (_Direction == "R")
                {
                    _Position.X += 3;
                    if (_Position.X >= 26 + (32 * 16)) _Direction = "L";
                }
                else
                {
                    _Position.X -= 3;
                    if (_Position.X <= 26 + (32 * 1)) _Direction = "R";
                }
                _TimerFrame = 0;
            }
        }

        /* Работа с private переменными */
        public void ToSet_Position(Vector2 _newPosition) { _Position = _newPosition; }

        public Vector2 ToGet_Position() { return _Position; }

    }


    class Bonus : Objects
    {
        private bool _fExist;
        public Bonus(Texture2D _newTexture, Vector2 _newPosition) : base(_newTexture, _newPosition)
        {
            _fExist = true;
        }
        public void Update()
        {
            if (_fExist)
            {
                _Rectangle = new Rectangle((int)_Position.X + _Texture.Width, (int)_Position.Y + _Texture.Height, _Texture.Width, _Texture.Height);
                _OriginalPosition = new Vector2(_Rectangle.Width, _Rectangle.Height);
                _RectangleInteraction = new Rectangle((int)_Position.X, (int)_Position.Y, _Texture.Width, _Texture.Height);
            }
        }
        public void Draw(SpriteBatch _SpriteBatch)
        {
            if (_fExist)
            {
                _SpriteBatch.Draw(_Texture, _Rectangle, null, _Colour, 0f, _OriginalPosition, SpriteEffects.None, 0.4f);
            }
        }


        /* Работа с private переменными */
        public void ToSet_fExist(bool _newfExist)
        {
            _fExist = _newfExist;
        }

        public bool ToGet_fExist()
        {
            return _fExist;
        }

    }
}
