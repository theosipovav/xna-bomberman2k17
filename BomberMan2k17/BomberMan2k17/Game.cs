﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;
using System.Xml;

namespace BomberMan2k17
{
    public class Game : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager _GraphicsDeviceManager;
        SpriteBatch _SpriteBatch;
        /* Глобальные переменные для управления игрой */
        /* Разделы игры */
        string _StageGame1, _StageGame2;
        /*
        Для _StageGame1:
        <Menu> - игровое меню
        <Game> - игровой процесс
        Для _StageGame2:
        <Menu>, <Start>, <Raiting>, <Exit> для _StageGame1 == Menu
        <1lvl>, <2lvl> для _StageGame1 == Game 
        */
        InputText _InputText;
        List<Buttons> _ListButtons = new List<Buttons>();
        Player _Player;
        Bomb _Bomb;
        Portal[] _Portal = new Portal[2];
        Bonus _Bonus;
        List<Block> _ListBlock = new List<Block>();
        List<BlockDestr> _ListBlockDestr = new List<BlockDestr>();
        List<Enemy> _ListEnemy = new List<Enemy>();
        float _Timer_1lvl, _Timer_2lvl;
        Song[] Sound = new Song[2];
        bool[] FlagStart = new bool[2];


        public Game()
        {
            _GraphicsDeviceManager = new GraphicsDeviceManager(this);
            _GraphicsDeviceManager.PreferredBackBufferWidth = 600;
            _GraphicsDeviceManager.PreferredBackBufferHeight = 800;
            IsMouseVisible = true;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            _InputText = new InputText(new Vector2(50, 230), Content.Load<SpriteFont>("SpriteFont_ComicSansMS"));
            Sound[0] = Content.Load<Song>("SoundMenu");
            Sound[1] = Content.Load<Song>("SoundPlay");
            FlagStart[0] = false;
            FlagStart[1] = false;
            _Timer_1lvl = 60;
            _Timer_2lvl = 120;
            _StageGame1 = "Menu";
            _StageGame2 = "Menu";
            _Portal[0] = new Portal(Content.Load<Texture2D>("portal"), new Vector2(458, 43));
            _Portal[1] = new Portal(Content.Load<Texture2D>("portal"), new Vector2(10 + (32 * 10), 10 + (32 * 12)));
            _ListButtons.Add(new Buttons(Content.Load<Texture2D>("btn1"), new Vector2(300, 400)));
            _ListButtons.Add(new Buttons(Content.Load<Texture2D>("btn2"), new Vector2(300, 500)));
            _ListButtons.Add(new Buttons(Content.Load<Texture2D>("btn3"), new Vector2(300, 600)));
            _ListButtons.Add(new Buttons(Content.Load<Texture2D>("btn4"), new Vector2(400, 750)));
            _ListButtons.Add(new Buttons(Content.Load<Texture2D>("btn4"), new Vector2(300, 700)));
            _ListButtons.Add(new Buttons(Content.Load<Texture2D>("btn5"), new Vector2(300, 600)));
            /* инициализация объектов для всех уровней */
            _Player = new Player(Content.Load<Texture2D>("player"), new Vector2(58, 58));
            _Bomb = new Bomb(Content.Load<Texture2D>("Bomb"), new Vector2(0, 0), Content.Load<SpriteFont>("SpriteFont_ComicSansMS"), Content.Load<Texture2D>("Boom"));
            /* Уставнока объектов для 1 уровня */
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 43)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 75)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 107)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 139)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 171)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 203)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 235)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 267)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 299)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 331)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 363)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 395)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 427)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 459)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 491)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 523)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 363)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(42, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(74, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(106, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(138, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(170, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(202, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(234, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(266, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(298, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(330, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(362, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(394, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(426, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(458, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(490, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(522, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(362, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 10)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 43)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 75)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 107)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 139)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 171)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 203)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 235)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 267)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 299)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 331)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 363)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 395)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 427)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 459)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 491)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 523)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 363)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(554, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(42, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(74, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(106, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(138, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(170, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(202, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(234, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(266, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(298, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(330, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(362, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(394, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(426, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(458, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(490, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(522, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(362, 555)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + 32, 107)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 2), 107)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 3), 107)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 4), 107)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 5), 107)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 6), 107)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 7), 107)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 8), 107)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 9), 107)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 10), 107)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(426, 10 + 32)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(426, 10 + (32 * 2))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(426, 10 + (32 * 3))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(426, 10 + (32 * 4))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(426, 10 + (32 * 5))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(426, 10 + (32 * 6))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(426, 10 + (32 * 7))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(426, 10 + (32 * 8))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(426, 10 + (32 * 9))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(426, 10 + (32 * 10))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 3), 203)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 4), 203)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 5), 203)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 6), 203)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 7), 203)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 8), 203)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 9), 203)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 10), 203)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 11), 203)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 12), 203)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + 32, 267)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 2), 267)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 3), 267)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 4), 267)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 5), 267)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 6), 267)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 7), 267)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 8), 267)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 9), 267)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 10), 267)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + 32, 331)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 2), 331)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 3), 331)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 4), 331)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 5), 331)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 6), 331)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 9), 331)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 10), 331)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 11), 331)));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 12), 331)));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 11), 107)));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 12), 107)));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 1), 203)));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 2), 203)));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 11), 267)));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 12), 267)));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 7), 331)));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 8), 331)));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(426, 10 + (32 * 11))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(426, 10 + (32 * 12))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(426, 10 + (32 * 13))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(426, 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(426, 10 + (32 * 15))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(426, 10 + (32 * 16))));
            /* Уставнока объектов для 2 уровня */
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 2), 10 + (32 * 2))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 4), 10 + (32 * 2))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 6), 10 + (32 * 2))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 8), 10 + (32 * 2))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 10), 10 + (32 * 2))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 12), 10 + (32 * 2))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 1), 10 + (32 * 4))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 3), 10 + (32 * 4))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 11), 10 + (32 * 4))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 13), 10 + (32 * 4))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 15), 10 + (32 * 4))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 2), 10 + (32 * 6))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 4), 10 + (32 * 6))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 6), 10 + (32 * 6))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 8), 10 + (32 * 6))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 10), 10 + (32 * 6))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 12), 10 + (32 * 6))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 14), 10 + (32 * 6))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 16), 10 + (32 * 6))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 1), 10 + (32 * 8))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 3), 10 + (32 * 8))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 5), 10 + (32 * 8))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 7), 10 + (32 * 8))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 9), 10 + (32 * 8))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 11), 10 + (32 * 8))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 13), 10 + (32 * 8))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 15), 10 + (32 * 8))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 2), 10 + (32 * 10))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 4), 10 + (32 * 10))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 6), 10 + (32 * 10))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 8), 10 + (32 * 10))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 10), 10 + (32 * 10))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 12), 10 + (32 * 10))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 14), 10 + (32 * 10))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 16), 10 + (32 * 10))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 1), 10 + (32 * 12))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 3), 10 + (32 * 12))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 5), 10 + (32 * 12))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 7), 10 + (32 * 12))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 15), 10 + (32 * 12))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 2), 10 + (32 * 15))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 4), 10 + (32 * 15))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 6), 10 + (32 * 15))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 8), 10 + (32 * 15))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 10), 10 + (32 * 15))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 12), 10 + (32 * 15))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 14), 10 + (32 * 15))));
            _ListBlock.Add(new Block(Content.Load<Texture2D>("b1"), new Vector2(10 + (32 * 16), 10 + (32 * 15))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 2), 10 + (32 * 4))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 4), 10 + (32 * 4))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 5), 10 + (32 * 4))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 6), 10 + (32 * 4))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 7), 10 + (32 * 4))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 8), 10 + (32 * 4))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 9), 10 + (32 * 4))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 10), 10 + (32 * 4))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 12), 10 + (32 * 4))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 14), 10 + (32 * 4))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 16), 10 + (32 * 4))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 1), 10 + (32 * 2))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 3), 10 + (32 * 2))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 5), 10 + (32 * 2))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 7), 10 + (32 * 2))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 3), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 4), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 5), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 6), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 7), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 8), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 9), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 10), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 11), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 12), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 13), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 14), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 15), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 16), 10 + (32 * 7))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 2), 10 + (32 * 8))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 4), 10 + (32 * 8))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 6), 10 + (32 * 8))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 8), 10 + (32 * 8))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 10), 10 + (32 * 8))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 12), 10 + (32 * 8))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 14), 10 + (32 * 8))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 16), 10 + (32 * 8))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 1), 10 + (32 * 9))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 2), 10 + (32 * 9))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 3), 10 + (32 * 9))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 4), 10 + (32 * 9))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 5), 10 + (32 * 9))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 1), 10 + (32 * 10))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 3), 10 + (32 * 10))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 5), 10 + (32 * 10))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 1), 10 + (32 * 11))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 2), 10 + (32 * 11))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 3), 10 + (32 * 11))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 4), 10 + (32 * 11))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 5), 10 + (32 * 11))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 2), 10 + (32 * 12))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 4), 10 + (32 * 12))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 1), 10 + (32 * 13))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 2), 10 + (32 * 13))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 3), 10 + (32 * 13))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 4), 10 + (32 * 13))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 5), 10 + (32 * 13))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 1), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 2), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 3), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 4), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 5), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 6), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 7), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 8), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 9), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 10), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 11), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 12), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 13), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 14), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 15), 10 + (32 * 14))));
            _ListBlockDestr.Add(new BlockDestr(Content.Load<Texture2D>("b2"), new Vector2(10 + (32 * 16), 10 + (32 * 14))));
            _ListEnemy.Add(new Enemy(Content.Load<Texture2D>("enemy"), new Vector2(26 + (32 * 5), 26 + (32 * 1))));
            _ListEnemy.Add(new Enemy(Content.Load<Texture2D>("enemy"), new Vector2(26 + (32 * 1), 26 + (32 * 3))));
            _ListEnemy.Add(new Enemy(Content.Load<Texture2D>("enemy"), new Vector2(26 + (32 * 15), 26 + (32 * 5))));
            _Bonus = new Bonus(Content.Load<Texture2D>("bonus"), new Vector2(10 + (32 * 16), 10 + (32 * 5)));





            base.Initialize();
        }

        protected override void LoadContent()
        {
            _SpriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime _GameTime)
        {
            MouseState _MouseState = Mouse.GetState();
            KeyboardState _KeyboardState = Keyboard.GetState();
            switch (_StageGame1)
            {
                case "Menu":
                    switch (_StageGame2)
                    {
                        case "Menu":
                            if (FlagStart[0] == false)
                            {
                                MediaPlayer.Play(Sound[0]);
                                MediaPlayer.IsRepeating = true;
                                MediaPlayer.Volume = 0.3f;
                                FlagStart[0] = true;
                            }
                            _ListButtons[0].Update(_MouseState);
                            _ListButtons[1].Update(_MouseState);
                            _ListButtons[2].Update(_MouseState);
                            if (_ListButtons[0].isClicked == true)
                            {
                                _StageGame2 = "Start";
                                _ListButtons[0].isClicked = false;
                            }
                            if (_ListButtons[1].isClicked == true)
                            {
                                _StageGame2 = "Raiting";
                                _ListButtons[1].isClicked = false;
                            }
                            if (_ListButtons[2].isClicked == true)
                            {
                                _StageGame2 = "Exit";
                                _ListButtons[2].isClicked = false;
                            }

                            break;
                        case "Start":
                            _InputText.Update(_GameTime);
                            _ListButtons[4].Update(_MouseState);
                            _ListButtons[5].Update(_MouseState);
                            if ((_ListButtons[4].isClicked == true) || (_KeyboardState.IsKeyDown(Keys.Escape))) _StageGame2 = "Menu";
                            if ((_ListButtons[5].isClicked == true) || (_KeyboardState.IsKeyDown(Keys.Enter)))
                            {
                                if ((_InputText.FullText.Length <= 10) && (_InputText.FullText.Length > 0))
                                {
                                    _Player.ToSet_Name(_InputText.FullText);
                                    _StageGame1 = "Game";
                                    _StageGame2 = "1lvl";
                                }
                            }
                            break;
                        case "Raiting":
                            _ListButtons[3].Update(_MouseState);
                            if ((_ListButtons[3].isClicked == true) || (_KeyboardState.IsKeyDown(Keys.Escape))) _StageGame2 = "Menu";
                            break;
                        case "Exit":
                            Exit();
                            break;
                    }
                    break;
                case "Game":
                    switch (_StageGame2)
                    {
                        case "1lvl":
                            if (FlagStart[1] == false)
                            {
                                MediaPlayer.Play(Sound[1]);
                                MediaPlayer.IsRepeating = true;
                                MediaPlayer.Volume = 0.3f;
                                FlagStart[1] = true;
                            }
                            _Player.Update(_GameTime);
                            _Bomb.Update(_GameTime);
                            _Portal[0].Update();
                            if (_Player.Interaction(_Portal[0].ToGet_RectangleInteraction()))
                            {
                                _StageGame2 = "2lvl";
                            }
                            if (_Player.ToGet_Live() <= 0)
                            {
                                _StageGame1 = "Game";
                                _StageGame2 = "FinalFail";
                            }
                            for (int n = 0; n < 122; n++)
                            {
                                _ListBlock[n].Update();
                                if (_Player.Interaction(_ListBlock[n].ToGet_RectangleInteraction()))
                                {
                                    _Player.StopVelosity();
                                }
                            }
                            for (int n = 0; n < 14; n++)
                            {
                                if (_ListBlockDestr[n].ToGet_fExist())
                                {
                                    _ListBlockDestr[n].Update();
                                    if (_Player.Interaction(_ListBlockDestr[n].ToGet_RectangleInteraction()))
                                    {
                                        _Player.StopVelosity();
                                    }
                                    if (_Bomb.ToGet_fBoom())
                                    {
                                        for (int i = 0; i < _Bomb.ToGet_TrackBomb().Length; i++)
                                        {
                                            if (_ListBlockDestr[n].Interaction(_Bomb.ToGet_TrackBomb()[i]))
                                            {
                                                _ListBlockDestr[n].Dest();
                                            }
                                        }
                                    }
                                    if (_Bomb.ToGet_fBoom())
                                    {
                                        for (int i = 0; i < _Bomb.ToGet_TrackBomb().Length; i++)
                                        {
                                            if (_Player.InteractionIn(_Bomb.ToGet_TrackBomb()[i]))
                                            {
                                                _Player.DMG(1);
                                            }
                                        }
                                    }
                                }
                            }
                            if (_KeyboardState.IsKeyDown(Keys.Space))
                            {
                                if (!_Bomb.ToGet_fActivate())
                                {
                                    _Bomb.ToSet_Position(new Vector2((int)_Player.ToGet_Position().X, (int)_Player.ToGet_Position().Y));
                                    _Bomb.ToSet_fActivate(true);
                                }
                            }
                            _Timer_1lvl -= (float)_GameTime.ElapsedGameTime.TotalSeconds;
                            if (_Timer_1lvl <= 0)
                            {
                                _StageGame1 = "Game";
                                _StageGame2 = "FinalFail";
                            }

                            break;
                        case "2lvl":
                            _Player.Update(_GameTime);
                            if (_Player.ToGet_Live() <= 0)
                            {
                                _StageGame1 = "Game";
                                _StageGame2 = "FinalFail";
                            }

                            if (_KeyboardState.IsKeyDown(Keys.Space))
                            {
                                if (!_Bomb.ToGet_fActivate())
                                {
                                    _Bomb.ToSet_Position(new Vector2((int)_Player.ToGet_Position().X, (int)_Player.ToGet_Position().Y));
                                    _Bomb.ToSet_fActivate(true);
                                }
                            }
                            _Bomb.Update(_GameTime);
                            _Portal[1].Update();
                            if (_Player.Interaction(_Portal[1].ToGet_RectangleInteraction()))
                            {
                                _StageGame1 = "Game";
                                _StageGame2 = "FinalWin";
                            }
                            for (int n = 0; n < 72; n++)
                            {
                                _ListBlock[n].Update();
                                if (_Player.Interaction(_ListBlock[n].ToGet_RectangleInteraction()))
                                {
                                    _Player.StopVelosity();
                                }
                            }
                            for (int n = 122; n < _ListBlock.Count; n++)
                            {
                                _ListBlock[n].Update();
                                if (_Player.Interaction(_ListBlock[n].ToGet_RectangleInteraction()))
                                {
                                    _Player.StopVelosity();
                                }
                            }
                            for (int n = 14; n < _ListBlockDestr.Count; n++)
                            {
                                if (_ListBlockDestr[n].ToGet_fExist())
                                {
                                    _ListBlockDestr[n].Update();
                                    if (_Player.Interaction(_ListBlockDestr[n].ToGet_RectangleInteraction()))
                                    {
                                        _Player.StopVelosity();
                                    }
                                    if (_Bomb.ToGet_fBoom())
                                    {
                                        for (int i = 0; i < _Bomb.ToGet_TrackBomb().Length; i++)
                                        {
                                            if (_ListBlockDestr[n].Interaction(_Bomb.ToGet_TrackBomb()[i]))
                                            {
                                                _ListBlockDestr[n].Dest();
                                            }
                                        }
                                    }
                                    if (_Bomb.ToGet_fBoom())
                                    {
                                        for (int i = 0; i < _Bomb.ToGet_TrackBomb().Length; i++)
                                        {
                                            if (_Player.InteractionIn(_Bomb.ToGet_TrackBomb()[i]))
                                            {
                                                _Player.DMG(1);
                                            }
                                        }
                                    }
                                }
                            }

                            for (int n = 0; n < _ListEnemy.Count; n++)
                            {
                                _ListEnemy[n].Update(_GameTime);
                                _ListEnemy[n].Movement(_GameTime);
                                if (_Player.Interaction(_ListEnemy[n].ToGet_RectangleInteraction()))
                                {
                                    _Player.DMG(1);
                                }
                            }
                            _Bonus.Update();
                            if (_Player.Interaction(_Bonus.ToGet_RectangleInteraction()))
                            {
                                if (_Bonus.ToGet_fExist())
                                {
                                    _Player.AddLive(1);
                                    _Bonus.ToSet_fExist(false);
                                }
                            }

                            _Timer_2lvl -= (float)_GameTime.ElapsedGameTime.TotalSeconds;
                            if (_Timer_2lvl <= 0)
                            {
                                _StageGame1 = "Game";
                                _StageGame2 = "FinalFail";
                            }
                            break;
                        case "FinalFail":
                            if (_KeyboardState.IsKeyDown(Keys.Enter))
                            {
                                _StageGame1 = "Menu";
                                _StageGame2 = "Exit";
                            }
                            break;
                        case "FinalWin":
                            if (_KeyboardState.IsKeyDown(Keys.Enter))
                            {
                                SaveResult(_Player.ToGet_Name(), (int)(_Timer_1lvl + _Timer_2lvl));
                                _StageGame1 = "Menu";
                                _StageGame2 = "Exit";
                            }
                            break;
                    }
                    break;
            }



            base.Update(_GameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            _SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null);
            switch (_StageGame1)
            {
                case "Menu":
                    _SpriteBatch.Draw(Content.Load<Texture2D>("background_menu"), new Rectangle(0, 0, 600, 800), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.99f);
                    switch (_StageGame2)
                    {
                        case "Menu":
                            _SpriteBatch.Draw(Content.Load<Texture2D>("logo"), new Rectangle(85, 50, 453, 206), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.89f);
                            _ListButtons[0].Draw(_SpriteBatch);
                            _ListButtons[1].Draw(_SpriteBatch);
                            _ListButtons[2].Draw(_SpriteBatch);
                            break;
                        case "Start":
                            _SpriteBatch.Draw(Content.Load<Texture2D>("start"), new Rectangle(0, 0, 600, 800), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.7f);
                            _ListButtons[4].Draw(_SpriteBatch);
                            _ListButtons[5].Draw(_SpriteBatch);
                            _InputText.Draw(_SpriteBatch);
                            break;
                        case "Raiting":
                            LoadResult();
                            _SpriteBatch.Draw(Content.Load<Texture2D>("raiting"), new Rectangle(0, 0, 600, 800), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.98f);
                            _ListButtons[3].Draw(_SpriteBatch);
                            break;
                        case "Exit":

                            break;
                    }
                    break;
                case "Game":
                    switch (_StageGame2)
                    {
                        case "1lvl":
                            _Player.Draw(_SpriteBatch);
                            _Bomb.Draw(_SpriteBatch);
                            _Portal[0].Draw(_SpriteBatch);
                            _SpriteBatch.Draw(Content.Load<Texture2D>("background_game"), new Rectangle(0, 0, 600, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.89f);

                            for (int n = 0; n < _ListBlock.Count; n++)
                            {
                                _ListBlock[n].Draw(_SpriteBatch);
                            }
                            for (int n = 0; n < _ListBlockDestr.Count; n++)
                            {
                                _ListBlockDestr[n].Draw(_SpriteBatch);
                            }
                            /*********************************************************************/
                            /* Вовд инфомрации на нижнюю часть экрана */
                            string[] _info = new string[5];
                            _SpriteBatch.DrawString(Content.Load<SpriteFont>("SpriteFont_ComicSansMS"), "Timer : " + ((int)_Timer_1lvl).ToString() + "s", new Vector2(250, 600), Color.White, 0f, new Vector2(0, 0), 0.2f, SpriteEffects.None, 0.5f);
                            _SpriteBatch.DrawString(Content.Load<SpriteFont>("SpriteFont_ComicSansMS"), "You name : " + _Player.ToGet_Name(), new Vector2(10, 630), Color.White, 0f, new Vector2(0, 0), 0.2f, SpriteEffects.None, 0.5f);
                            _SpriteBatch.DrawString(Content.Load<SpriteFont>("SpriteFont_ComicSansMS"), "Live: " + _Player.ToGet_Live(), new Vector2(500, 630), Color.White, 0f, new Vector2(0, 0), 0.2f, SpriteEffects.None, 0.5f);
                            /*********************************************************************/
                            break;
                        case "2lvl":
                            _Player.Draw(_SpriteBatch);
                            _Bomb.Draw(_SpriteBatch);
                            _Portal[1].Draw(_SpriteBatch);
                            _SpriteBatch.Draw(Content.Load<Texture2D>("background_game"), new Rectangle(0, 0, 600, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.89f);
                            for (int n = 0; n < 72; n++)
                            {
                                _ListBlock[n].Draw(_SpriteBatch);
                            }
                            for (int n = 122; n < _ListBlock.Count; n++)
                            {
                                _ListBlock[n].Draw(_SpriteBatch);
                            }
                            for (int n = 14; n < _ListBlockDestr.Count; n++)
                            {
                                _ListBlockDestr[n].Draw(_SpriteBatch);
                            }

                            for (int n = 0; n < _ListEnemy.Count; n++)
                            {
                                _ListEnemy[n].Draw(_SpriteBatch);
                            }
                            _Bonus.Draw(_SpriteBatch);



                            /*********************************************************************/
                            /* Вовд инфомрации на нижнюю часть экрана */
                            _info = new string[5];
                            _SpriteBatch.DrawString(Content.Load<SpriteFont>("SpriteFont_ComicSansMS"), "Timer : " + ((int)_Timer_2lvl).ToString() + "s", new Vector2(250, 600), Color.White, 0f, new Vector2(0, 0), 0.2f, SpriteEffects.None, 0.5f);
                            _SpriteBatch.DrawString(Content.Load<SpriteFont>("SpriteFont_ComicSansMS"), "You name : " + _Player.ToGet_Name(), new Vector2(10, 630), Color.White, 0f, new Vector2(0, 0), 0.2f, SpriteEffects.None, 0.5f);
                            _SpriteBatch.DrawString(Content.Load<SpriteFont>("SpriteFont_ComicSansMS"), "Live: " + _Player.ToGet_Live(), new Vector2(500, 630), Color.White, 0f, new Vector2(0, 0), 0.2f, SpriteEffects.None, 0.5f);
                            
                            /*********************************************************************/
                            break;
                        case "FinalFail":
                            _SpriteBatch.Draw(Content.Load<Texture2D>("background_FinalFail"), new Rectangle(0, 0, 600, 800), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.89f);
                            break;
                        case "FinalWin":
                            _SpriteBatch.Draw(Content.Load<Texture2D>("background_FinalWin"), new Rectangle(0, 0, 600, 800), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.89f);
                            _SpriteBatch.DrawString(Content.Load<SpriteFont>("SpriteFont_Arial"), ((int)(_Timer_1lvl + _Timer_2lvl)).ToString(), new Vector2(360, 297), Color.White, 0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0.5f);
                            break;
                    }
                    break;
            }
            _SpriteBatch.End();
            base.Draw(gameTime);
        }

        private void SaveResult(string _newName, int _newXP)
        {
            string _PlayerDate = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
            string _CurrentDirectory = Environment.CurrentDirectory;
            FileStream _FileStream;
            XmlDocument _XmlDocument;
            if (File.Exists(_CurrentDirectory + @"\Save.xml"))
            {
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Open);
                _XmlDocument = new XmlDocument();
                _XmlDocument.Load(_FileStream);
                _FileStream.Close();
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Create);
                if (_XmlDocument["Game"].ChildNodes.Count <= 5)
                {
                    XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                    XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                    _XmlAttribute1.Value = _newName;
                    _XmlNode.Attributes.Append(_XmlAttribute1);
                    XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                    _XmlAttribute2.Value = _newXP.ToString();
                    _XmlNode.Attributes.Append(_XmlAttribute2);
                    XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                    _XmlAttribute3.Value = _PlayerDate;
                    _XmlNode.Attributes.Append(_XmlAttribute3);
                    _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                }
                else
                {
                    _XmlDocument["Game"].RemoveAll();
                    XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                    XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                    _XmlAttribute1.Value = _newName;
                    _XmlNode.Attributes.Append(_XmlAttribute1);
                    XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                    _XmlAttribute2.Value = _newXP.ToString();
                    _XmlNode.Attributes.Append(_XmlAttribute2);
                    XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                    _XmlAttribute3.Value = _PlayerDate;
                    _XmlNode.Attributes.Append(_XmlAttribute3);
                    _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                }
                _XmlDocument.Save(_FileStream);
                _FileStream.Close();
            }
            else
            {
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.CreateNew);
                _XmlDocument = new XmlDocument();
                _XmlDocument.LoadXml("<Game></Game>");
                XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                _XmlAttribute1.Value = _newName;
                _XmlNode.Attributes.Append(_XmlAttribute1);
                XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                _XmlAttribute2.Value = _newXP.ToString();
                _XmlNode.Attributes.Append(_XmlAttribute2);
                XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                _XmlAttribute3.Value = _PlayerDate;
                _XmlNode.Attributes.Append(_XmlAttribute3);
                _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                _XmlDocument.Save(_FileStream);
                _FileStream.Close();
            }
        }

        private void LoadResult()
        {
            string _CurrentDirectory = Environment.CurrentDirectory;
            if (File.Exists(_CurrentDirectory + @"\Save.xml"))
            {
                string _PlayerName = "";
                string _PlayerXP = "";
                string _PlayerDate = "";
                int _interval = 50;
                FileStream _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Open);
                XmlDocument _XmlDocument = new XmlDocument();
                _XmlDocument.Load(_FileStream);
                _FileStream.Close();
                for (int i = 0; i < _XmlDocument["Game"].ChildNodes.Count; i++)
                {
                    _PlayerName = _XmlDocument["Game"].ChildNodes[i].Attributes["Name"].InnerText;
                    _PlayerXP = _XmlDocument["Game"].ChildNodes[i].Attributes["XP"].InnerText;
                    _PlayerDate = _XmlDocument["Game"].ChildNodes[i].Attributes["Date"].InnerText;
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("SpriteFont_ComicSansMS"), _PlayerName, new Vector2(100, 100 + _interval), Color.White, 0f, new Vector2(0, 0), 0.3f, SpriteEffects.None, 0.5f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("SpriteFont_ComicSansMS"), _PlayerXP, new Vector2(450, 100 + _interval), Color.White, 0f, new Vector2(0, 0), 0.3f, SpriteEffects.None, 0.5f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("SpriteFont_ComicSansMS"), _PlayerDate, new Vector2(700, 100 + _interval), Color.White, 0f, new Vector2(0, 0), 0.3f, SpriteEffects.None, 0.5f);

                    _interval += 50;
                }
            }
        }
    }
}
